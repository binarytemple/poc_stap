#!/usr/bin/env stap
# Show sockets setting options

# Return enabled or disabled based on value of optval
function getstatus(optval)
{
    if ( optval == 1 )
        return "enabling"
    else
        return "disabling"
}

probe begin
{
        print ("\nChecking for apps setting socket options\n")
}

# Set a socket option
probe tcp.setsockopt
{
    status = getstatus(user_int($optval))
    printf ("  App '%s' (PID %d) is %s socket option %s... %s ", execname(), pid(), status, optstr, cmdline_args(0,-1," " ))
}

# Check setting the socket option worked
probe tcp.setsockopt.return
{
    if ( ret == 0 )
        printf ("success")
    else
        printf ("failed")
    printf ("\n")
}

probe end
{
        print ("\nClosing down\n")
}
